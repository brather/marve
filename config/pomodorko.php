<?php
/**
 * Created by PhpStorm.
 * User: a.chechel
 * Date: 05.03.17
 * Time: 15:04
 */

$params = require(__DIR__ . '/params.php');

$config = [
	'id' => 'basic',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'components' => [
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => '7racbbNQGz-7Z1EYH1D4mRsR0h6wEQU3',
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'user' => [
			'identityClass' => 'app\models\User',
			'enableAutoLogin' => true,
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => true,
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'db' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=localhost;dbname=pomodoro',
			'username' => 'root',
			'password' => 'q245nzSR',
			'charset' => 'utf8',
		],
		'urlManager' => [
			'showScriptName'=>false,
			'enablePrettyUrl' => true,
			'rules' => [
				['class' => 'yii\rest\UrlRule', 'controller' => 'task'],
				'<controller:[\w-]+>' => '<controller>/index',
				'DELETE <controller:[\w-]+>/<action:\w+>/<id:[\w\d]+>' => '<controller>/remove',
				'<controller:[\w-]+>/<action:\w+>' => '<controller>/<action>',
			],
		],
	],
	'params' => $params
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
	];

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
		'allowedIPs' => ["*"]
	];
}

return $config;
