<?php

$nextNumberA = 0;
$nextNumberB = 0;
$nextNumberC = 0;
$currNumber  = 0;
/* @var $this   yii\web\View
 * @var $res    $this->res
 */
?>
<style>
    .column-placeholder {
        border: 2px dotted #f3f3f3;
        margin: 0 1em 1em 0;
        height: 150px;
    }
    .column-toggle {
        position: absolute;
        margin-top: -8px;
        top: 50%;
        right: 0;

    }
</style>
<div class="clearfix row">
    <div class="js-grid sortable grid clearfix ml4" aria-dropeffect="move" data-sortable-id="1">
    <? foreach ($res as $k => $task):?>
        <?
        switch ($task->WEIGHT)
        {
            case "A":
                $nextNumberA += 1;
                $currNumber   = $nextNumberA;
                break;
            case "B":
                $nextNumberB += 1;
                $currNumber   = $nextNumberB;
                break;
            case "C":
                $nextNumberC += 1;
                $currNumber   = $nextNumberC;
                break;
        }

        ?>
        <div class="col-lg-4 col-xs-12 col-sm-6 column px2 py1" aria-grabbed="false" role="option" draggable="true" data-item-sortable-id="1">
            <div class="task-container">
                <div class="weight-<?= mb_strtolower($task->WEIGHT) ?>">
                    <span class="weight"><?= $task->WEIGHT ?></span>
                    <span class="number"><?= $currNumber ?></span>
                </div>
                <div class="task-title">
                    <span class="title"><?= $task->TITLE ?></span>
                    <div class="dropdown">
                        <a data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-th-list"></span></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <li><span
                                    class="edit-task"
                                    data-id="<?=$task->ID?>"
                                    data-sort="<?= $task->SORT ?>"
                                ><img src="/img/icon edit.png"></span></li>
                            <li><img src="/img/icon time.png"></li>
                            <li><span class="remove-task" data-id="<?=$task->ID?>"><img src="/img/icon trash.png"></span></li>
                        </ul>
                    </div>
                </div>
                <div class="task-body">
                    <?= $task->TEXT ?>
                </div>
            </div>
        </div>

    <? endforeach;?>
    </div>
</div>

<div class="add-task" data-toggle="modal" data-target=".modal-sm-create" >
    <div class="round-1">
        <div class="round-2">
            <span class="glyphicon glyphicon-plus"></span>
        </div>
    </div>
</div>
<div class="modal fade modal-sm-create" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form role="form" action="/tasks/create" method="post">
            <div class="modal-header">
                Создать новую задачу
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-5 col-xs-5">
                        <select name="WEIGHT" class="form-control" id="weight" >
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                        </select>
                    </div>
                    <div class="col-lg-7 col-xs-7"><label for="weight">Приоритет</label></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-12 col-xs-12">
                        <input type="text" name="TITLE" placeholder="TITLE" class="form-control" />
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-12">
                        <textarea class="form-control" name="TEXT" id="task" cols="30" rows="8" placeholder="Текст задачи"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit">Создать</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade modal-sm-edit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabelEdit" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form id="taskEdit" onsubmit="return false;" role="form">
                <input type="hidden" name="ID" value="" />
                <input type="hidden" name="SORT" value="" />
            <div class="modal-header">
                Изменить задачу
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-5 col-xs-5">
                        <select name="WEIGHT" class="form-control" id="weight" >
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                        </select>
                    </div>
                    <div class="col-lg-7 col-xs-7"><label for="weight">Приоритет</label></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-12 col-xs-12">
                        <input type="text" name="TITLE" placeholder="TITLE" class="form-control" />
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-12">
                        <textarea class="form-control" name="TEXT" id="task" cols="30" rows="8" placeholder="Текст задачи"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-task-submit" type="submit">Edit</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    (function(d,w,$){
        sortable(".js-grid", {
            forcePlaceholderSize: true,
            placeholder: '<div class="col-lg-4 col-xs-12 col-sm-6"><div class="border border-maroon"><h2 class="center">Drop here</h2></div></div>',
            dragImage: null
        });

        $("#weight").on("change", function () {
            var value   =  this.value,
                $prNum  = $("#pr_number"),
                num     = $prNum.data(value.toLowerCase());

            $prNum.html('');

            for (var i = 1; i < 6; i++){
                var tagOption   = d.createElement("option");

                tagOption.value = num + i;
                tagOption.textContent = num + i;

                d.getElementById('pr_number').appendChild(tagOption);
            }
        });

        $("span.remove-task").on('click', function () {
            var id = $(this).data('id');

            $.ajax({
                url: '/tasks/remove?id=' + id ,
                method: 'DELETE',
                data: {ID: id},
                success: function(res){
                    if(res == 1)
                        w.location.reload();
                },
                error: function(xth, st,msg){
                    console.error(st, msg);
                }
            });
        });

        $("span.edit-task").on('click', function () {
            var $this   = $(this),
                id      = $this.data('id'),
                taskEdit = $("div.modal-sm-edit"),
                thisTaskContainer = $this.parents("div.task-container"),
                data    = {};

            data.taskBody   = thisTaskContainer.find("div.task-body").text().trim();
            data.taskBody   = thisTaskContainer.find("div.task-body").text().trim();
            data.taskId     = id;
            data.sort       = $this.data('sort');
            data.taskTitle  = thisTaskContainer.find("span.title").text().trim();
            data.taskWeight = thisTaskContainer.find("span.weight").text();

            taskEdit.find("input[name=ID]").val(data.taskId);
            taskEdit.find("input[name=SORT]").val(data.sort);
            taskEdit.find("textarea[name=TEXT]").val(data.taskBody);
            taskEdit.find("select[name=WEIGHT]").val(data.taskWeight);
            taskEdit.find("input[name=TITLE]").val(data.taskTitle);

            taskEdit.modal("show");
        });

        $("button.btn-task-submit").on("click", function (e) {
            e.preventDefault();
            e.stopPropagation();

            var formData = d.getElementById("taskEdit"),
                data = {};

            for(var i = 0; i < formData.elements.length; i++){
                if (formData.elements[i].type != "submit"){
                    data[formData.elements[i].name] = formData.elements[i].value;
                }
            }

            $.ajax({
                url: '/tasks/' + data['ID'] + '/',
                data: data,
                method: 'PUT',
                dataType: 'json',
                success: function(res){
                    console.log(res);
                },
                error: function (xhr, st, msg) {
                    console.error(st, msg);
                }
            })
        });

    })(document,window,jQuery)
</script>


