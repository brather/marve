<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 22.03.17
 * Time: 19:51
 */
?>
<div class="clearfix row">
    <div class="col-lg-12">
        <h2 class="center">Цели</h2>
    </div>
</div>
<div class="clearfix row">
    <div class="col-xs-6 col-xs-offset-3">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default border-left-a">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Приобретение квартиры
                            <span id="element_1" class="pull-right glyphicon glyphicon-plus" data-toggle="collapse" data-target="#element_panel_1"></span>
                        </h4>
                    </div>
                    <div class="pull-right date-marker">от 24.12.2017</div>
                </div>
                <div id="element_panel_1" class="collapse" style="margin-top: 36px;">
                    <div class="panel-group">
                        <div class="panel panel-tasks">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <span class="weight-panel weight-panel-a pull-left">&nbsp;</span>
                                    Поиск подработки
                                    <span class="pull-right glyphicon glyphicon-plus"></span>
                                </h4>
                            </div>
                        </div>
                        <div class="panel panel-tasks">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <span class="weight-panel weight-panel-a pull-left">&nbsp;</span>
                                    Поиск подработки
                                    <span class="pull-right glyphicon glyphicon-plus"></span>
                                </h4>
                            </div>
                        </div>
                        <div class="panel panel-tasks">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <span class="weight-panel weight-panel-a pull-left">&nbsp;</span>
                                    Поиск подработки
                                    <span class="pull-right glyphicon glyphicon-plus"></span>
                                </h4>
                            </div>
                        </div>
                        <div class="panel panel-tasks">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <span class="weight-panel weight-panel-a pull-left">&nbsp;</span>
                                    Поиск подработки
                                    <span class="pull-right glyphicon glyphicon-plus"></span>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="panel panel-default border-left-a">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Приобретение квартиры
                            <span id="element_2" class="pull-right glyphicon glyphicon-plus" ></span>
                        </h4>
                    </div>
                    <div class="pull-right date-marker">от 24.12.2017</div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    (function(d, $){
        $('#element_panel_1').on('shown.bs.collapse', function () {
            $('#element_1')
                    .removeClass('glyphicon-plus')
                    .addClass('glyphicon-minus');
        }).on('hidden.bs.collapse', function () {
                $('#element_1')
                    .addClass('glyphicon-plus')
                    .removeClass('glyphicon-minus');
        });
    })(document, jQuery);
</script>
