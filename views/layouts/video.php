<?php
/**
 * Created by PhpStorm.
 * User: a.chechel
 * Date: 11.02.17
 * Time: 18:03
 *
 * @var $this \yii\web\View
 * @var $content string
 */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
	<div class="container">
		<?= $content ?>
		<? var_dump($this); ?>
	</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>