<?php
/**
 * Created by PhpStorm.
 * User: a.chechel
 * Date: 11.02.17
 * Time: 18:03
 *
 * @var $this \yii\web\View
 * @var $content string
 */
use app\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
	<!doctype html>
	<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<?php $this->head() ?>
		<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" />
		<link rel="stylesheet" href="/css/bootstrap-theme.css" type="text/css" />
		<link rel="stylesheet" href="/css/style.css" type="text/css" />
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/html.sortable.min.js"></script>
	</head>
	<body>
	<?php $this->beginBody() ?>
	<header>
		<nav class="navbar navbar-default navbar-static-top" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-8">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-8">
				<ul class="nav navbar-nav">
					<li <?= $this->context->id == 'main' ? 'class="active"' : ''?>><a href="/">Home</a></li>
					<li <?= $this->context->id == 'target' ? 'class="active"' : ''?>><a href="/target">Target</a></li>
					<li <?= $this->context->id == 'tasks' ? 'class="active"' : ''?>><a href="/tasks">Tasks</a></li>
					<li <?= $this->context->id == 'stat' ? 'class="active"' : ''?>><a href="#">Statistics</a></li>
					<li <?= $this->context->id == 'about' ? 'class="active"' : ''?>><a href="/about">About as</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</nav>
	</header>
	<div class="container">

		<?= $content ?>
	</div>
	<?php $this->endBody() ?>
	</body>
	</html>
<?php $this->endPage() ?>