<?php

namespace app\controllers;

use Yii;

class VideoController extends \yii\web\Controller
{
    public $arResult = [];

	public $layout = 'video';

    public function actionDownload()
    {
        return $this->render('download');
    }

    public function actionGraber()
    {
        return $this->render('graber');
    }

    public function actionIndex()
    {
        return $this->render('index', array(
            'db' => Yii::$app->db
        ));
    }

    public function actionProtected()
    {
        return $this->render('protected');
    }

    public function actionPublic()
    {
        return $this->render('public');
    }

    public function actionRecord()
    {
        return $this->render('record');
    }

    public function actionSubscribe()
    {
        return $this->render('subscribe');
    }

    public function actionVote()
    {
        return $this->render('vote');
    }

    public function actionWishList()
    {
        return $this->render('wish-list');
    }

}
