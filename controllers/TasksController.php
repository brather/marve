<?php

namespace app\controllers;

use app\models\BTasks;
use yii\web\Request;

class TasksController extends \yii\web\Controller
{
	public $layout = 'pomodoro';


    public function actionIndex()
    {
	    $res = BTasks::find()
		    ->orderBy(["WEIGHT" => SORT_ASC, "SORT" => SORT_ASC])
		    ->all();

        return $this->render('@app/views/pomodoro/tasks', ["res" => $res]);
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    public function actionUpdate()
    {
        if (!$this->_getReq()->isPut) die('false');
        else die('true');

        $isUpdate = BTasks::updateAll(
            [
                "SORT" => $this->_getReq()->post('SORT'),
                "TITLE" => $this->_getReq()->post("TITLE"),
                "TEXT" => $this->_getReq()->post("TEXT"),
                "WEIGHT" => $this->_getReq()->post("WEIGHT")
            ],
            ["ID" => $this->_getReq()->post("ID")]);

        if ($isUpdate > 0)
            die(json_encode(["status" => "ok", "update_row" => $isUpdate ]));
        else
            die(json_encode(["status" => "error", "msg" => $isUpdate]));
    }

    public function actionCreate()
    {
		if (!$this->_isPost()) die('false');

        $bt = new BTasks();

        // get MAX SORT value ...
        $max = BTasks::find()
            ->where(['WEIGHT' => (string)$this->_getReq()->post('WEIGHT')])
            ->orderBy(['SORT' => SORT_DESC])
            ->one()
            ->SORT;

        $bt->SORT   = $max + 100;
	    $bt->WEIGHT = $this->_getReq()->post('WEIGHT');
	    $bt->TITLE  = $this->_getReq()->post('TITLE');
	    $bt->TEXT   = $this->_getReq()->post('TEXT');

        if ($bt->save() !== true)
        {
            die(var_dump($bt->getErrors()));
        }
        else {
            $this->redirect('/tasks/');
        }
	    return 0;
    }

	public function actionRemove( )
	{
		// todo Need check permission
		if ( !$this->_isDelete() ) die('false');

		$id = $this->_getReq()->get("id");

		$isRemove = BTasks::findOne( (int)$id )->delete();

		if ( $isRemove !== false )
		{
			return $isRemove;
		}

		return false;
	}

	private function _getDb()
	{
		return \Yii::$app->getDb();
	}

	/* @return Request */
    private function _getReq()
    {
        return \Yii::$app->getRequest();
    }

    private function _isPost()
    {
	    return $this->_getReq()->isPost;
    }

    private function _isDelete()
    {
	    return $this->_getReq()->isDelete;
    }


	public function beforeAction($action)
	{
		// ...set `$this->enableCsrfValidation` here based on some conditions...
		// call parent method that will check CSRF if such property is true.
		if ($action->id === 'create' || $action->id === 'remove') {
			# code...
			$this->enableCsrfValidation = false;
		}
		return parent::beforeAction($action);
	}

}


