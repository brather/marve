<?php
/**
 * Created by PhpStorm.
 * User: a.chechel
 * Date: 22.01.17
 * Time: 22:50
 */

namespace app\controllers;

use Codeception\Lib\Driver\Db;
use Yii;
use yii\base\Controller;

class AdminController  extends Controller {
	private function _getDb()
	{
		return Yii::$app->getDb();
	}
}