<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.03.17
 * Time: 12:16
 */

namespace app\controllers;

use app\models\BTasks;
use yii\web\Request;

class AjaxController extends \yii\web\Controller
{
    public function actionTasksort ()
    {
        $marginLeftId = BTasks::find()
            ->where(array('ID' => $this->getRq()->post('LEFT_MARGIN_ID')))
            ->one();

        die(var_export($marginLeftId));

        $bt = BTasks::updateAll(
            array(
                'SORT' => $this->getRq()->post('SORT')
            ),
            array(
                ''
            )
        );






        die('error');
    }

    /**
     * @return \yii\web\Request mixed
     */
    private function getRq()
    {
        return \Yii::$app->getRequest();
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        // if header is not ajax, return this message
        if ( !$this->getRq()->isAjax ) die('is not ajax');

        // actions controller which not have render views
        $actionCsrfFalse = [
            'tasksort',
        ];
        // ...set `$this->enableCsrfValidation` here based on some conditions...
        // call parent method that will check CSRF if such property is true.
        if (in_array($action->id, $actionCsrfFalse, true)) {
            # code...
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
}