<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18.03.17
 * Time: 16:10
 */

namespace app\controllers;

class AboutController extends \yii\web\Controller
{
    public $layout = 'pomodoro';

    public function actionIndex()
    {
        return $this->render('@app/views/pomodoro/about', []);
    }
}