<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "b_tasks".
 *
 * @property integer $ID
 * @property integer $SORT
 * @property string $WEIGHT
 * @property string $TITLE
 * @property string $TEXT
 */
class BTasks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'b_tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SORT'], 'integer'],
            [['TEXT'], 'string'],
            [['WEIGHT'], 'string', 'max' => 5],
            [['TITLE'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SORT' => 'Order sort',
            'WEIGHT' => 'Weight',
            'TITLE' => 'Title',
            'TEXT' => 'Text',
        ];
    }
}
